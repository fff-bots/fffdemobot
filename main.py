#!/usr/bin/env python
# -*- coding: utf-8 -*-
import config
import time
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
import requests

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

url = 'https://fff-api.dmho.de/v1/scrape/list'
response = requests.get(url)

response_json = response.json()


def demos(bot, update):

    update.message.reply_text('Hier sind die Demos für nächsten Freitag:')
    message = ""
    message2 = ""
    for city in response_json['list']:
        message = message + (city['city']+"\n"+city['time']+"\n"+city['place']+"\n-----------------"+"\n")

    update.message.reply_text(message)
    bot.send_message(config.adminuserid, "Jemand (dank DSGVO) hat "+"/demos ausgeführt.")



def help(bot, update):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Commands:\n/demos')


def echo(bot, update):
    update.message.reply_text("Sorry, das hab ich nicht verstanden.")
    help(bot, update)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(config.token)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("demos", demos))
    dp.add_handler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
